import { IsOptional } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { Tweet } from '../tweet.model';

export class AttachTweeMetaSchema {
  @ApiProperty()
  @IsOptional()
  mentions: string[];

  @ApiProperty()
  @IsOptional()
  hashtags: string[];

  @ApiProperty()
  @IsOptional()
  tweetId: string;
}

export class TransformTweetTextSchema {
  @ApiProperty()
  @IsOptional()
  text: string;

  @IsOptional()
  tweet: Tweet;
}
