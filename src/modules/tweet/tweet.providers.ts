import {
  Retweet,
  Tweet,
  TweetComment,
  TweetView,
  TweetHashtag,
  TweetHashtags,
  TweetMentions,
} from './tweet.model';
import {
  RETWEET_REPOSITORY,
  TWEET_COMMENT_REPOSITORY,
  TWEET_REPOSITORY,
  TWEET_VIEW_REPOSITORY,
  TWEET_HASHTAGS_REPOSITORY,
  TWEETS_HASHTAGS_REPOSITORY,
  TWEET_MENTIONS_REPOSITORY,
} from '@core/constants/repository';

export const tweetProviders = [
  {
    provide: TWEET_REPOSITORY,
    useValue: Tweet,
  },

  {
    provide: TWEET_COMMENT_REPOSITORY,
    useValue: TweetComment,
  },
  {
    provide: RETWEET_REPOSITORY,
    useValue: Retweet,
  },

  {
    provide: TWEET_VIEW_REPOSITORY,
    useValue: TweetView,
  },
  {
    provide: TWEET_HASHTAGS_REPOSITORY,
    useValue: TweetHashtag,
  },
  {
    provide: TWEETS_HASHTAGS_REPOSITORY,
    useValue: TweetHashtags,
  },
  {
    provide: TWEET_MENTIONS_REPOSITORY,
    useValue: TweetMentions,
  },
];
