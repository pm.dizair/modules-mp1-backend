import { forwardRef, Inject, Injectable } from '@nestjs/common';
import { Tweet } from '../tweet.model';
import { TweetService } from './tweet';
import { ATTACH } from '@core/constants/attach';
import {
  AttachSchemaType,
  TweetAttachMediaSchema,
  TweetAttachProjectSchema,
  TweetAttachVideoSchema,
} from '../dto/attach.schema';
import { ProjectService } from 'src/modules/project/services/project.service';
import { EventService } from 'src/modules/event/event.service';
import { TweetDTO } from '../tweet.helpers';
import { PROJECT_LOWER } from '@core/constants/project';
import {
  DATABASE_ENUM_TYPE,
  PRODUCT_LOWER_ENUM_TYPE,
  PROJECT_LOWER_ENUM_TYPE,
} from '@core/constants/enum.types';
import { TweetMetaService } from './tweet-meta';
import { DaoService } from '@modules/dao/services/dao.service';
import { ProductService } from '@modules/product/services/product.service';
import { PRODUCT_LOWER } from '@core/constants';
import { getUrls } from '@modules/web_scan/web_scan.helpers';

@Injectable()
export class TweetAttachService {
  constructor(
    @Inject(forwardRef(() => DaoService))
    private readonly daoService: DaoService,
    @Inject(forwardRef(() => TweetService))
    private readonly tweetService: TweetService,
    @Inject(forwardRef(() => EventService))
    private readonly eventService: EventService,
    @Inject(forwardRef(() => ProjectService))
    private readonly projectService: ProjectService,
    @Inject(forwardRef(() => ProductService))
    private readonly productService: ProductService,
    private readonly tweetMetaService: TweetMetaService
  ) {}

  async findAllAndAttach(
    entityId: string,
    entityType: DATABASE_ENUM_TYPE,
    offset: number,
    limit: number,
    userId: string = null
  ): Promise<{
    rows: Tweet[];
    count: number;
  }> {
    let tweets = [];

    const tweetsDb = await this.tweetService.findAndCountAll(
      entityId,
      entityType,
      offset,
      limit,
      userId
    );

    for (const tweetDb of tweetsDb.rows) {
      const { attach, attachObject } = await this.getAttach(tweetDb, userId);
      tweets.push(TweetDTO(tweetDb, attach, attachObject));
    }

    tweets = await this.tweetMetaService.transformTweets(tweets);

    return {
      rows: tweets,
      count: tweetsDb.count,
    };
  }

  async getAttach(tweet: Tweet, userId: string, retweet = 0) {
    const getAttachItem = async (attach: AttachSchemaType) => {
      if (
        Object.values(PROJECT_LOWER).includes(
          attach.objectType as PROJECT_LOWER_ENUM_TYPE
        )
      ) {
        return {
          attach,
          attachObject: await this.projectService.findOneProject(
            (attach as TweetAttachProjectSchema).objectId,
            userId
          ),
        };
      }

      if (
        Object.values(PRODUCT_LOWER).includes(
          attach.objectType as PRODUCT_LOWER_ENUM_TYPE
        )
      ) {
        return {
          attach,
          attachObject: await this.productService.findOneProduct(
            (attach as TweetAttachProjectSchema).objectId,
            userId
          ),
        };
      }

      if (
        [ATTACH.MEDIA, ATTACH.VIDEO].includes(
          (attach as TweetAttachMediaSchema | TweetAttachVideoSchema).objectType
        )
      )
        return {
          attach,
          attachObject: attach,
        };

      switch (attach.objectType) {
        case ATTACH.DAO:
          return {
            attach,
            attachObject: await this.daoService.findOneDAO(
              attach.objectId,
              userId
            ),
          };
        case ATTACH.EVENT:
          return {
            attach,
            attachObject: await this.eventService.findOne(attach.id, userId),
          };
        case ATTACH.NFT:
          return { attach, attachObject: null };
        default:
          return { attach: null, attachObject: null };
      }
    };

    if (tweet.retweetId) {
      const sharedTweet = await this.tweetService.getById(
        tweet.retweetId,
        userId
      );
      return await this.getAttach(sharedTweet, userId, retweet + 1);
    }

    if (tweet.attach) {
      if (retweet > 0) {
        const { attach, attachObject } = await getAttachItem(tweet.attach);

        return {
          attach: null,
          attachObject: TweetDTO(tweet, attach, attachObject),
        };
      }

      return await getAttachItem(tweet.attach);
    }

    const url = getUrls(tweet.text)?.[0];

    if (url) {
      return { attach: { url, objectType: ATTACH.LINK }, attachObject: null };
    }

    return {
      attach: null,
      attachObject: {
        ...tweet.get(),
        attach: null,
        attachObject: null,
      },
    };
  }
}
