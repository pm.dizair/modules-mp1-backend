import { AbiItem } from 'web3-utils';

export default [
  {
    inputs: [
      {
        internalType: 'address',
        name: 'dao_address',
        type: 'address',
      },
    ],
    name: 'getCoverageByDaoAddress',
    outputs: [
      {
        internalType: 'uint256',
        name: '',
        type: 'uint256',
      },
    ],
    stateMutability: 'view',
    type: 'function',
  },
] as AbiItem[];
