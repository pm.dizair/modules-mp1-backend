import { ApiBearerAuth, ApiConsumes, ApiTags } from '@nestjs/swagger';
import {
  Body,
  Controller,
  DefaultValuePipe,
  Delete,
  Get,
  HttpException,
  HttpStatus,
  NotFoundException,
  Param,
  ParseIntPipe,
  Post,
  Put,
  Query,
  Request,
  UploadedFile,
  UseGuards,
  Ip,
  UseInterceptors,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { CloudinaryService } from '@core/utils/cloudinary/cloudinary.service';
import { JwtAuthGuard } from '@auth/jwt.guard';
import { CommentSchema, UpdateCommentSchema } from '../dto/comment.schema';
import { TweetService } from '../tweet.service/tweet';
import { TweetCommentService } from '../tweet.service/comment';
import SOCKET from '@core/constants/socket';
import { EntityGateway } from '@modules/general/general.gateway';
import { NotBuilderService } from '@modules/notification/services/notification.builder';
import { PromotionService } from '@modules/promotion/service/promotion.service';
import { PromotionInteractionService } from '@modules/promotion/service/promotion-interaction.service';

@ApiTags('tweet')
@Controller('tweet/:id')
export class CommentController {
  constructor(
    private readonly notCreatorService: NotBuilderService,
    private readonly commentService: TweetCommentService,
    private readonly cloudinary: CloudinaryService,
    private readonly tweetService: TweetService,
    private readonly gate: EntityGateway,
    private readonly promotionService: PromotionService,
    private readonly promotionInteractionService: PromotionInteractionService
  ) {}

  @UseGuards(JwtAuthGuard)
  @Get('comment')
  async commentAll(
    @Param('id') id: string,
    @Query('page', new DefaultValuePipe(1), ParseIntPipe) page: number,
    @Query('limit', new DefaultValuePipe(10), ParseIntPipe) limit: number
  ) {
    return this.commentService.findAndCountAll(id, page, limit);
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @UseInterceptors(FileInterceptor('file'))
  @ApiConsumes('multipart/form-data')
  @Post('comment')
  async addComment(
    @Request() req,
    @Param('id') id: string,
    @Body() body: CommentSchema,
    @UploadedFile() file: Express.Multer.File,
    @Ip() userIp: string
  ) {
    const userId = req.user.userId;
    const userAgent = req.headers['user-agent'];

    let media = null;
    if (file) {
      const { secure_url } = await this.cloudinary.uploadImage(file);
      media = secure_url;
    }

    const tweet = await this.tweetService.getSimpleById(id);
    const promotion = await this.promotionService.getByTweetId(id);

    const comment = await this.commentService.create(
      tweet.id,
      body,
      userId,
      media
    );

    if (promotion) {
      const { entityType, entityId } = body;

      await this.promotionInteractionService.commentInteraction(
        entityType,
        entityId,
        tweet.id,
        req.user.userId,
        userIp,
        userAgent
      );
    }

    if (tweet.userId !== userId)
      this.notCreatorService.commentedTweet(tweet, comment, userId);

    const channels = await this.tweetService.findAllEntityChannels(tweet.id);

    for (const { entityId, entityType } of channels) {
      const commentsCount = await this.commentService.getCommentsCount(
        tweet.id
      );
      this.gate.server
        .to(`entity_${entityId}_${entityType}`)
        .emit(SOCKET.COMMENT_RECEIVE, tweet.id, comment, commentsCount);
    }
    return comment;
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @Put('comment/:comment_id')
  async editComment(
    @Request() req,
    @Param('id') id: string,
    @Param('comment_id') comment_id: string,
    @Body() body: UpdateCommentSchema
  ) {
    const { numberOfAffectedRows, updatedRow } =
      await this.commentService.update(comment_id, body, req.user.userId);
    if (numberOfAffectedRows === 0) {
      throw new NotFoundException("This comment doesn't exist");
    }
    return updatedRow;
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @Delete('comment/:comment_id')
  async deleteComment(
    @Request() req,
    @Param('id') id: string,
    @Param('comment_id') comment_id: string
  ) {
    const comment = await this.commentService.getByIdAndUser(
      comment_id,
      req.user.userId
    );
    if (!comment) {
      throw new HttpException('No comment found', HttpStatus.NOT_FOUND);
    }
    if (comment.media) {
      await this.cloudinary.removeImage(comment.media);
    }

    return this.commentService.delete(comment_id);
  }
}
