import { AnonimOrJwtGuard, JwtAuthGuard } from '@core/auth/jwt.guard';
import { ATTACH } from '@core/constants/attach';
import {
  Body,
  Controller,
  Get,
  Post,
  Request,
  UseGuards,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { LikeService } from '../social/services/social.like';
import { UserService } from '../user/user.service';
import { InfoSchema, NFTSchema } from './dto/nft.dto';
import { NftMoralisService } from './services/nft.moralis';
import { NftService } from './services/nft.service';
import { ShareNFTSchema } from '../social/dto/share.dto';
import { NotBuilderService } from '../notification/services/notification.builder';
import { DATABASE_CONSTANTS } from '@core/constants/database';

@ApiTags('nft')
@Controller('nft')
export class NftController {
  constructor(
    private readonly nftService: NftService,
    private readonly userService: UserService,
    private readonly likeService: LikeService,
    private readonly nftMoralisService: NftMoralisService,
    private readonly notCreatorService: NotBuilderService
  ) {}

  @Get('liked')
  async getLiked() {
    return await this.nftService.getMostLiked();
  }

  @UseGuards(JwtAuthGuard)
  @Post('like')
  async like(@Request() req, @Body() body: NFTSchema) {
    const userId = req.user.userId;

    const [nftDB] = await this.nftService.findOrCreate(body);

    await this.likeService.createLikeNotification(
      nftDB.id,
      DATABASE_CONSTANTS.NFT,
      userId
    );

    return await this.likeService.like(nftDB.id, ATTACH.NFT, userId);
  }

  @UseGuards(JwtAuthGuard)
  @Post('share')
  async shareNft(@Request() req, @Body() body: ShareNFTSchema) {
    const userId = req.user.userId;

    const tweets = await this.nftService.shareNFT(body, userId);

    try {
      const nft = await this.nftMoralisService.fetchNFTFromMoralis(
        body.nft.tokenAddress,
        body.nft.tokenId
      );

      const user = await this.userService.findByPublicAddress(
        nft.item.owner_of
      );

      if (user && userId !== user.id)
        this.notCreatorService.sharedNFT(nft, user.id, userId);
    } catch (e) {
      console.log(e);
    }

    return tweets.length;
  }

  @UseGuards(AnonimOrJwtGuard)
  @Post()
  async getNfts(@Request() req, @Body() body: InfoSchema) {
    return this.nftService.findNfts(body.ids, req.user?.userId);
  }
}
