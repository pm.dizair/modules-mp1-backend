import { DATABASE_CONSTANTS } from '@core/constants/database';
import sequelize, { ProjectionAlias } from 'sequelize';
import { NFTSchema } from 'src/modules/nft/dto/nft.dto';
import { includeLikeExist, includeLikesCount } from '../social/social.helpers';

export const includeLikeExistNFT = (
  nft: NFTSchema,
  userId: string | undefined
) => {
  const args: ProjectionAlias[] = [
    includeLikesCount(DATABASE_CONSTANTS.NFT),
    [
      sequelize.literal(`(
        SELECT 
          COUNT (*) 
        FROM 
          tweets 
        WHERE 
          tweets."attach" IS NOT NULL 
          AND tweets."attach" :: JSONB ->> 'objectType' = '${DATABASE_CONSTANTS.NFT}' 
          AND tweets."attach" :: JSONB ->> 'tokenAddress' = '${nft.tokenAddress}' 
          AND tweets."attach" :: JSONB ->> 'tokenId' = '${nft.tokenId}'
      )`),
      'shareCount',
    ],
  ];
  if (userId) args.push(includeLikeExist(DATABASE_CONSTANTS.NFT, userId));
  return args;
};
