import { DATABASE_CONSTANTS } from '@core/constants/database';
import { Column, DataType, HasMany, Model, Table } from 'sequelize-typescript';
import { Like } from '../social/social.model';

@Table({ tableName: 'nfts', updatedAt: false })
export class Nft extends Model {
  @Column({
    defaultValue: DataType.UUIDV4,
    primaryKey: true,
    type: DataType.UUID,
  })
  id: string;
  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  tokenAddress: string;
  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  tokenId: string;

  @HasMany(() => Like, {
    constraints: true,
    foreignKey: 'entityId',
    scope: {
      entityType: DATABASE_CONSTANTS.NFT,
    },
  })
  likes: Like[];
}
