import { NFTMoralisSchema } from './../dto/nft.moralis.dto';
import { Injectable } from '@nestjs/common';
import { GeneralFetchService } from '@modules/general/services/fetch.service';

@Injectable()
export class NftMoralisService {
  constructor(private readonly generalFetchService: GeneralFetchService) {}

  async fetchNFTFromMoralis(
    address: string,
    token_id: string,
    owner_of: string = null
  ): Promise<NFTMoralisSchema | null> {
    return await this.generalFetchService.fetchFromMoralis('details', {
      address,
      token_id,
      owner_of,
    });
  }
}
