import * as Sentry from '@sentry/node';
import {
  Tweet,
  TweetComment,
  TweetHashtags,
  TweetMentions,
  TweetView,
} from '../tweet.model';
import {
  SEQUELIZE_REPOSITORY,
  TWEETS_HASHTAGS_REPOSITORY,
  TWEET_COMMENT_REPOSITORY,
  TWEET_MENTIONS_REPOSITORY,
  TWEET_REPOSITORY,
  TWEET_VIEW_REPOSITORY,
} from '@core/constants/repository';
import {
  Attributes,
  FindAndCountOptions,
  FindOptions,
  Optional,
  Sequelize,
  WhereOptions,
} from 'sequelize';
import { defaultOptions, getTweetMeta, TweetDTO } from '../tweet.helpers';
import { ATTACH } from '@core/constants/attach';
import { CloudinaryService } from '@core/utils/cloudinary/cloudinary.service';
import { CreateTweetSchema } from '../dto/tweet.schema';
import {
  BadRequestException,
  forwardRef,
  Inject,
  Injectable,
  InternalServerErrorException,
} from '@nestjs/common';
import { includeMinUser } from 'src/modules/user/user.helpers';
import { TweetAttachService } from './attach';
import { PRODUCT_LOWER, PROJECT_LOWER } from '@core/constants';
import { TweetActionService } from './action';
import {
  ADVANCED_ENTITIES_ENUM_TYPE,
  DATABASE_ENUM_TYPE,
} from '@core/constants/enum.types';
import { TweetMetaService } from './tweet-meta';
import { Op } from 'sequelize';
import { S3 } from 'aws-sdk';
import { ConfigService } from '@nestjs/config';
import { TweetCacheService } from './cache.service';
import { AttachSchemaType } from '../dto/attach.schema';
import ffmpeg from 'fluent-ffmpeg';
import { Readable } from 'stream';
import {
  IMAGE_SIZE_LIMIT,
  VIDEO_DURATION_LIMIT_SECOND,
  VIDEO_SIZE_LIMIT_MB,
} from '@core/constants/media';
import sharp from 'sharp';

@Injectable()
export class TweetService {
  constructor(
    @Inject(SEQUELIZE_REPOSITORY)
    private readonly sequelize: Sequelize,
    @Inject(TWEET_REPOSITORY)
    private readonly tweetModel: typeof Tweet,
    @Inject(TWEET_VIEW_REPOSITORY)
    private readonly tweetViewModel: typeof TweetView,
    @Inject(TWEET_COMMENT_REPOSITORY)
    private readonly tweetCommentModel: typeof TweetComment,
    @Inject(TWEET_MENTIONS_REPOSITORY)
    private readonly tweetMentionsModel: typeof TweetMentions,
    @Inject(TWEETS_HASHTAGS_REPOSITORY)
    private readonly tweetHashtagsModel: typeof TweetHashtags,
    @Inject(forwardRef(() => TweetAttachService))
    private readonly tweetAttachService: TweetAttachService,
    @Inject(forwardRef(() => TweetActionService))
    private readonly tweetActionService: TweetActionService,
    private readonly cloudinaryService: CloudinaryService,
    private readonly tweetMetaService: TweetMetaService,
    private readonly cacheService: TweetCacheService,
    private readonly configService: ConfigService
  ) {}

  async getRetweetsCount(id: string) {
    const { count } = (
      await this.sequelize.query(`(
        SELECT 
          COUNT(*) 
        FROM 
          tweets 
        WHERE 
          tweets."retweetId" = '${id}'
      )`)
    )[0][0] as { count: number };

    return Number(count);
  }

  async findAllEntityChannels(id: string) {
    const results = (await this.tweetModel.findAll({
      where: {
        [Op.or]: [
          {
            id,
          },
          {
            retweetId: id,
          },
        ],
      },
      attributes: ['entityId', 'entityType'],
    })) as { entityId: string; entityType: DATABASE_ENUM_TYPE }[];

    const channels: { entityId: string; entityType: DATABASE_ENUM_TYPE }[] = [];

    for (const item of results) {
      if (
        !channels.find(
          ({ entityId, entityType }) =>
            item.entityId === entityId && item.entityType === entityType
        )
      )
        channels.push(item);
    }

    return channels;
  }

  async findAll(
    entityId: string,
    entityType: string,
    page: number,
    limit: number,
    userId: string = null,
    ownerId: string = null
  ): Promise<Tweet[]> {
    let args: FindOptions<Tweet> = {
      limit,
      ...defaultOptions(userId),
      offset: (page - 1) * limit,
      where: {
        entityId,
        entityType,
      },
    };
    if (ownerId) {
      args = {
        ...args,
        where: {
          ...args.where,
          userId: ownerId,
        },
      };
    }
    return await this.tweetModel.findAll(args);
  }

  async findAndCountAll(
    entityId: string,
    entityType: DATABASE_ENUM_TYPE,
    offset: number,
    limit: number,
    userId: string = null,
    ownerId: string = null,
    whereOptions: WhereOptions<Tweet> = {}
  ): Promise<{
    rows: Tweet[];
    count: number;
  }> {
    let args: FindAndCountOptions<Tweet> = {
      limit,
      offset,
      ...defaultOptions(userId),
      where: {
        entityId,
        entityType,
        ...whereOptions,
      },
    };
    if (ownerId) {
      args = {
        ...args,
        where: {
          ...args.where,
          userId: ownerId,
        },
      };
    }

    return await this.tweetModel.findAndCountAll(args);
  }

  async getSimpleById(id: string): Promise<Tweet> {
    return this.tweetModel.findOne({
      where: { id },
    });
  }

  async getById(id: string, userId: string = null): Promise<Tweet> {
    return await this.tweetModel.findOne({
      ...defaultOptions(userId),
      where: { id },
      include: [
        includeMinUser({ required: true }),
        includeMinUser({ as: 'mentions' }),
      ],
    });
  }

  async getByIdAndAttach(id: string, userId: string) {
    const tweet = await this.getById(id, userId);
    const { attach, attachObject } = await this.tweetAttachService.getAttach(
      tweet,
      userId
    );

    let attachWithText = attachObject;

    if (tweet.retweetId) {
      attachWithText = {
        ...(attachObject || {}),
        display_text: await this.tweetMetaService.transformTweetText(
          attachObject
        ),
      };
    }

    const text = await this.tweetMetaService.transformTweetText(tweet);

    return { ...TweetDTO(tweet, attach, attachWithText), display_text: text };
  }

  async getByIdAndUser(id: string, userId: string): Promise<Tweet> {
    return this.tweetModel.findOne({ where: { id, userId } });
  }

  async getTweets(
    entityId: string,
    entityType: DATABASE_ENUM_TYPE,
    offset: number,
    limit: number,
    userId: string,
    ownerId: string = null
  ): Promise<[string[], { count: number; rows: Tweet[] }]> {
    const { rows: tweets } = await this.findAndCountAll(
      entityId,
      entityType,
      offset,
      limit,
      userId,
      ownerId
    );

    const { rows, count } = await this.tweetAttachService.findAllAndAttach(
      entityId,
      entityType,
      offset,
      limit,
      userId
    );

    const payload = {
      count,
      rows: [
        ...rows.sort(
          (a, b) =>
            new Date(b.createdAt).getTime() - new Date(a.createdAt).getTime()
        ),
      ],
    };

    return [
      await this.tweetActionService.updateIsView(
        tweets.map((tweet) => tweet.id),
        userId
      ),
      payload,
    ];
  }

  async create(
    entityId: string,
    entityType: ADVANCED_ENTITIES_ENUM_TYPE,
    body: CreateTweetSchema,
    mediaFiles: Express.Multer.File[],
    userId: string
  ): Promise<Tweet> {
    let data: Optional<Tweet, keyof Tweet> = {
      userId,
      entityId,
      entityType,
      attach: null,
      text: body?.text,
      attachName: body.attach
        ? JSON.parse(body.attach || '{}')?.åattachName
        : '',
    };

    if (mediaFiles?.length) {
      const urls = [];

      for (const media of mediaFiles) {
        const res = await this.cloudinaryService.uploadImage(media);
        urls.push(res.url);
      }

      data = {
        ...data,
        attach: {
          objectType: 'media',
          urls: urls,
        },
      };
    } else if (body?.attach) {
      const attach: AttachSchemaType = JSON.parse(body?.attach);
      if (attach) {
        if (
          !Object.values({
            ...ATTACH,
            ...PROJECT_LOWER,
            ...PRODUCT_LOWER,
          }).includes(attach?.objectType)
        )
          throw new BadRequestException(`Type ${attach.objectType} is invalid`);

        data = {
          ...data,
          attachName: data.attachName?.toLowerCase(),
          attach: JSON.parse(body?.attach),
        };
      }
    }

    const tweet = await this.tweetModel.create<Tweet>(data);
    const { hashtags, mentions } = getTweetMeta(body.text);

    await this.tweetMetaService.attachTweetMetadata({
      hashtags,
      mentions,
      tweetId: tweet.id,
    });

    await this.tweetViewModel.findOrCreate({
      where: { tweetId: tweet.id, userId },
    });

    return tweet;
  }

  async update(id: string, data: Attributes<Tweet>, userId: string) {
    const [numberOfAffectedRows, [updatedRow]] = await this.tweetModel.update(
      { ...data, isEdited: true },
      { where: { id, userId }, returning: true }
    );
    return { numberOfAffectedRows, updatedRow };
  }

  async delete(id: string) {
    return this.sequelize.transaction(async (transaction) => {
      await this.tweetViewModel.destroy({
        where: { tweetId: id },
        transaction,
      });
      await this.tweetCommentModel.destroy({
        where: { tweetId: id },
        transaction,
      });
      await this.tweetMentionsModel.destroy({
        where: { tweetId: id },
        transaction,
      });
      await this.tweetHashtagsModel.destroy({
        where: { tweetId: id },
        transaction,
      });
      return await this.tweetModel.destroy({
        where: { id },
        transaction,
      });
    });
  }

  async uploadVideo(
    entityId: string,
    entityType: ADVANCED_ENTITIES_ENUM_TYPE,
    userId: string,
    text: string | null,
    video: Express.Multer.File,
    preview: Express.Multer.File | null
  ) {
    const s3 = new S3();

    const cache_key = await this.cacheService.cacheTweet(
      entityId,
      entityType,
      userId,
      text,
      video.size
    );

    let key = null;
    let previewBuffer = null;

    if (preview) key = `cache_key:${cache_key}|timestamp:0`;
    else key = `cache_key:${cache_key}|timestamp:1`;

    {
      if (video.size / 10 ** 6 > VIDEO_SIZE_LIMIT_MB)
        throw new BadRequestException(
          `This video cannot be loaded: Max size: ${VIDEO_SIZE_LIMIT_MB}GB`
        );
    }

    if (preview) {
      const image = sharp(preview.buffer);
      const metadata = await image.metadata();

      if (
        metadata.width < IMAGE_SIZE_LIMIT.MIN_WIDTH ||
        metadata.height < IMAGE_SIZE_LIMIT.MIN_HEIGHT
      )
        throw new BadRequestException(
          `Image min resolution must be 640x480, current: ${metadata.width}x${metadata.height}`
        );

      if (
        metadata.width > IMAGE_SIZE_LIMIT.MAX_WIDTH ||
        metadata.height > IMAGE_SIZE_LIMIT.MAX_HEIGHT
      ) {
        previewBuffer = await image
          .resize(IMAGE_SIZE_LIMIT.MAX_WIDTH, IMAGE_SIZE_LIMIT.MAX_HEIGHT)
          .jpeg()
          .toBuffer();
      } else previewBuffer = preview.buffer;
    }

    {
      const readable = Readable.from(video.buffer);
      const stream = ffmpeg(readable);

      const duration = await new Promise((resolve, reject) => {
        stream.ffprobe((err, data) => {
          if (err) {
            reject(null);
          } else {
            try {
              const duration = data.format.duration;
              if (duration) resolve(data.format.duration);
            } catch (e) {
              reject(null);
            }
          }
        });
      });

      if (!duration)
        throw new InternalServerErrorException(
          'Video duration must be greater than 0'
        );

      if (duration > VIDEO_DURATION_LIMIT_SECOND)
        throw new BadRequestException(
          `This video cannot be loaded: Max duration: ${VIDEO_DURATION_LIMIT_SECOND}s`
        );
    }

    {
      s3.upload({
        Bucket: this.configService.get('AWS_VIDEO_BUCKET_NAME'),
        Key: key,
        Body: video.buffer,
      }).promise();

      if (preview) {
        const previewKey = `video/${cache_key}/cover.jpg`;

        await s3
          .upload({
            Bucket: this.configService.get('AWS_IMAGE_BUCKET_NAME'),
            Key: previewKey,
            Body: previewBuffer,
          })
          .promise();
      }
    }

    return cache_key;
  }

  async removeCachedVideo(cache_key: string) {
    const cachedTweet = await this.cacheService.removeCached(cache_key);

    if (cachedTweet) {
      return cachedTweet;
    }

    return null;
  }

  async updateVideoStatus(cache_key: string) {
    const cachedTweet = await this.cacheService.getCachedTweet(cache_key);

    //for testing
    {
      const secondsLeft = Math.round(
        (Date.now() - cachedTweet.startTime) / 1000
      );

      const days = Math.floor(secondsLeft / 24 / 60 / 60);
      const hoursLeft = Math.floor(secondsLeft - days * 86400);
      const hours = Math.floor(hoursLeft / 3600);
      const minutesLeft = Math.floor(hoursLeft - hours * 3600);
      const minutes = Math.floor(minutesLeft / 60);
      const seconds = secondsLeft % 60;
      Sentry.captureMessage(
        `VIDEO STATUS. 
      SIZE: ${(cachedTweet.size / 10 ** 6).toFixed(2)} MB.
      TIME: ${days}:${hours}:${minutes}:${seconds}.`
      );
    }

    if (cachedTweet) {
      const attach: AttachSchemaType = {
        objectType: 'video',
        url: cache_key,
      };

      const { entityId, entityType, userId, text } = cachedTweet;
      const tweet = await this.tweetModel.create({
        entityType,
        entityId,
        userId,
        attach,
        text,
      });

      const { hashtags, mentions } = getTweetMeta(text);

      await this.tweetMetaService.attachTweetMetadata({
        hashtags,
        mentions,
        tweetId: tweet.id,
      });

      await this.tweetViewModel.findOrCreate({
        where: { tweetId: tweet.id, userId },
      });

      return tweet;
    }
  }
}
