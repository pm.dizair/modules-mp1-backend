import {
  Body,
  Controller,
  Delete,
  ForbiddenException,
  Get,
  HttpException,
  HttpStatus,
  Ip,
  NotFoundException,
  Param,
  Post,
  Put,
  Request,
  UseGuards,
} from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { JwtAuthGuard } from '@auth/jwt.guard';
import { CloudinaryService } from '@core/utils/cloudinary/cloudinary.service';
import { CreateTweetSchema, RefreshTweetSchema } from '../dto/tweet.schema';
import { TweetService } from '../tweet.service/tweet';
import { ATTACH } from '@core/constants/attach';
import SOCKET from '@core/constants/socket';
import { TweetActionService } from '../tweet.service/action';
import { ShareTweetBodySchema } from 'src/modules/social/dto/share.dto';
import { GeneralService } from 'src/modules/general/services/general.service';
import { EntityGateway } from '@modules/general/general.gateway';
import { NotBuilderService } from '@modules/notification/services/notification.builder';
import { DATABASE_CONSTANTS } from '@core/constants/database';
import { TweetRefreshGuard } from '../tweet.guard';
import { PromotionInteractionService } from '@modules/promotion/service/promotion-interaction.service';
import { PromotionService } from '@modules/promotion/service/promotion.service';
import { PROMOTION_ENTITIES_LOWER_ENUM_TYPE } from '@core/constants/enum.types';
@ApiTags('tweet')
@Controller('tweet')
export class TweetController {
  constructor(
    private readonly notCreatorService: NotBuilderService,
    private readonly tweetActionService: TweetActionService,
    private readonly generalService: GeneralService,
    private readonly cloudinary: CloudinaryService,
    private readonly tweetService: TweetService,
    private readonly gate: EntityGateway,
    private readonly promotionInteractionService: PromotionInteractionService,
    private readonly promotionService: PromotionService
  ) {}

  @Get(':id')
  async view(@Param('id') id: string) {
    return await this.tweetService.getById(id);
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @Put(':id/update')
  async edit(
    @Request() req,
    @Param('id') id: string,
    @Body() body: CreateTweetSchema
  ) {
    const userId = req.user.userId;
    const { numberOfAffectedRows, updatedRow } = await this.tweetService.update(
      id,
      body,
      userId
    );

    if (numberOfAffectedRows === 0) {
      throw new NotFoundException("This tweet doesn't exist");
    }

    this.gate.server
      .to(`entity_${updatedRow.entityId}_${updatedRow.entityType}`)
      .emit(
        SOCKET.TWEET_EDIT,
        await this.tweetService.getByIdAndAttach(updatedRow.id, userId)
      );

    return updatedRow;
  }

  @UseGuards(TweetRefreshGuard)
  @Post('refresh')
  async updateVideoStatus(@Body() body: RefreshTweetSchema) {
    const tweet = await this.tweetService.updateVideoStatus(body.cache);
    if (!tweet) return;

    const { id, userId, entityId, entityType } = tweet;
    const entity = await this.generalService.findOneAdvancedEntity(
      entityId,
      entityType
    );

    if (entity.userId !== userId && entityType !== DATABASE_CONSTANTS.PRODUCT) {
      this.notCreatorService.createdTweet(tweet, entity.userId, userId);
    }

    this.gate.server
      .to(`entity_${entityId}_${entityType}`)
      .emit(SOCKET.TWEET_VIDEO_STATUS, id, body.cache);

    this.gate.server
      .to(`entity_${entityId}_${entityType}`)
      .emit(
        SOCKET.TWEET_RECEIVE,
        await this.tweetService.getByIdAndAttach(id, userId)
      );
  }

  @UseGuards(TweetRefreshGuard)
  @Post('convert-fail')
  async convertFail(@Body() body: RefreshTweetSchema) {
    const tweet = await this.tweetService.removeCachedVideo(body.cache);
    if (!tweet) return;

    const { entityId, entityType } = tweet;

    this.gate.server
      .to(`entity_${entityId}_${entityType}`)
      .emit(SOCKET.REMOVE_CACHED_TWEET, body.cache);
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @Put(':id/pin')
  async pin(@Request() req, @Param('id') id: string) {
    const tweet = await this.tweetService.getSimpleById(id);
    const entity = await this.generalService.findOneAdvancedEntity(
      tweet.entityId,
      tweet.entityType
    );

    if (!entity.userId === req.user.userId)
      throw new ForbiddenException('You aren`nt pin this tweet');
    const result = await this.tweetActionService.pin(id);

    if (result === 1)
      this.gate.server
        .to(`entity_${tweet.entityId}_${tweet.entityType}`)
        .emit(SOCKET.TWEET_PIN, await this.tweetService.getById(id));
    else
      this.gate.server
        .to(`entity_${tweet.entityId}_${tweet.entityType}`)
        .emit(SOCKET.TWEET_UNPIN, await this.tweetService.getById(id));

    return result;
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @Delete(':id')
  async delete(@Request() req, @Param('id') id: string) {
    const userId = req.user.userId;
    const tweet = await this.tweetService.getSimpleById(id);
    const entity = await this.generalService.findOneAdvancedEntity(
      tweet.entityId,
      tweet.entityType
    );

    const isAccess = tweet.userId === userId || entity?.userId === userId;
    if (!isAccess)
      if (!tweet) {
        throw new ForbiddenException('Not access');
      }

    if (tweet?.attach?.objectType === ATTACH.MEDIA)
      for (const url of tweet.attach.urls)
        await this.cloudinary.removeImage(url);

    const result = await this.tweetService.delete(id);

    this.gate.server
      .to(`entity_${tweet.entityId}_${tweet.entityType}`)
      .emit(SOCKET.TWEET_REMOVE, id);

    return result;
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @Post(':id/retweet')
  async quote(
    @Request() req,
    @Param('id') id: string,
    @Body() body: ShareTweetBodySchema,
    @Ip() userIp: string
  ) {
    const userId = req.user.userId;
    const userAgent = req.headers['user-agent'];

    const tweet = await this.tweetService.getSimpleById(id);
    if (!tweet)
      throw new HttpException('Tweet not found', HttpStatus.NOT_FOUND);

    const tweets = await this.tweetActionService.retweetOrQuote(
      id,
      body,
      userId
    );

    if (tweet.userId !== userId)
      this.notCreatorService.sharedTweet(tweet, tweet.userId, userId);

    const { entityType, entityId } = body;

    for (const tweet of tweets) {
      await this.promotionInteractionService.repostInteraction(
        entityId,
        entityType as PROMOTION_ENTITIES_LOWER_ENUM_TYPE,
        id,
        userId,
        userIp,
        userAgent
      );

      this.gate.server
        .to(`entity_${tweet.entityId}_${tweet.entityType}`)
        .emit(
          SOCKET.TWEET_RECEIVE,
          await this.tweetService.getByIdAndAttach(tweet.id, userId)
        );
    }

    this.gate.server
      .to(`entity_${tweet.entityId}_${tweet.entityType}`)
      .emit(
        SOCKET.TWEET_RETWEET,
        tweet.id,
        await this.tweetService.getRetweetsCount(tweet.id)
      );

    return tweets;
  }
}
