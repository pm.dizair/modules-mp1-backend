import { NFT_SALE_ENUM_TYPE } from '@core/constants/enum.types';

export class NFTMetadataSchema {
  animation_url: string;
  image: string;
  name: string;
}

export class NFTSaleSchema {
  highest_bid: number;
  type: NFT_SALE_ENUM_TYPE;
}

export class NFTItemSchema {
  metadata: NFTMetadataSchema;
  token_address: string;
  token_id: string;
  owner_of: string;
}

export class NFTMoralisSchema {
  item: NFTItemSchema;
  sale: NFTSaleSchema;
}
