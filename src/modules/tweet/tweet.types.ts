import { Tweet } from "./tweet.model";

export interface TweetWithPromotionId extends Tweet {
    promotionId: string
}