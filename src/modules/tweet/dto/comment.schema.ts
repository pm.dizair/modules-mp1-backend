import { IsNotEmpty, IsUUID } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CommentSchema {
  @ApiProperty()
  @IsNotEmpty()
  text: string;

  @ApiProperty()
  @IsUUID()
  entityId: string;

  @ApiProperty()
  @IsNotEmpty()
  entityType: string;
}

export class UpdateCommentSchema {
  @ApiProperty()
  @IsNotEmpty()
  text: string;

  id: string;
}
