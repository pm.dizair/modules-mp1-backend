import { forwardRef, Inject, Injectable } from '@nestjs/common';
import { Tweet } from '../tweet.model';
import {
  TWEET_HASHTAGS_REPOSITORY,
  TWEETS_HASHTAGS_REPOSITORY,
  TWEET_MENTIONS_REPOSITORY,
} from '@core/constants/repository';
import { UserService } from '@modules/user/user.service';
import { AttachTweeMetaSchema } from '../dto/meta.schema';
import { TweetHashtag, TweetHashtags, TweetMentions } from '../tweet.model';
import { transformTweetMessage } from '../tweet.helpers';
import { TweetAttachSchema } from '../dto/tweet.schema';
import { PromotionService } from '@modules/promotion/service/promotion.service';

@Injectable()
export class TweetMetaService {
  constructor(
    @Inject(TWEET_HASHTAGS_REPOSITORY)
    private readonly tweetHashtagModel: typeof TweetHashtag,
    @Inject(TWEETS_HASHTAGS_REPOSITORY)
    private readonly tweetHashtagsModel: typeof TweetHashtags,
    @Inject(TWEET_MENTIONS_REPOSITORY)
    private readonly tweetMentionsModel: typeof TweetMentions,
    @Inject(forwardRef(() => UserService))
    private readonly userService: UserService,
    @Inject(forwardRef(() => PromotionService))
    private readonly promotionService: PromotionService
  ) {}

  async attachTweetMetadata(dto: AttachTweeMetaSchema) {
    await this.userService.findAndCheckForUsersExists(dto?.mentions || []);

    dto?.hashtags?.forEach(async (hashtag: string) => {
      const [newHashtag] = await this.tweetHashtagModel.findOrCreate({
        where: { name: hashtag },
      });

      await this.tweetHashtagsModel.create({
        hashtagId: newHashtag.id,
        tweetId: dto.tweetId,
      });
    });

    dto?.mentions?.forEach(async (mention: string) => {
      const user = await this.userService.findByPublicAddress(mention);

      await this.tweetMentionsModel.create({
        userId: user.id,
        tweetId: dto.tweetId,
      });
    });

    return dto.tweetId;
  }

  async transformTweetText({ text, mentions }: Tweet): Promise<string> {
    const usersDict = mentions.reduce((acc, item) => {
      return { ...acc, [item.publicAddress]: item?.nickname };
    }, {});

    const newMessage = transformTweetMessage(text, usersDict);

    return newMessage;
  }

  async transformTweets(tweets: Tweet[]): Promise<Tweet[]> {
    const transformedTweets = [];

    for (const tweet of tweets) {
      const transformedTweet = await this.transformTweet(tweet);
      transformedTweets.push(transformedTweet);
    }

    return transformedTweets as Tweet[];
  }

  async transformTweet(tweet: TweetAttachSchema) {
    let newTweet = {
      ...tweet,
      display_text: await this.transformTweetText(tweet),
    } as TweetAttachSchema;

    const promotion = await this.promotionService.getByTweetId(tweet.id);

    if (newTweet.retweetId && tweet.attachObject) {
      newTweet = {
        ...newTweet,
        isPromoted: !!promotion,
        attachObject: {
          ...tweet.attachObject,
          display_text: await this.transformTweetText(tweet.attachObject),
        },
      } as TweetAttachSchema;
    }

    return { ...newTweet, isPromoted: !!promotion };
  }
}
