import { forwardRef, Module } from '@nestjs/common';
import { CloudinaryModule } from '@core/utils/cloudinary/cloudinary.module';
import { tweetProviders } from './tweet.providers';
import { UserModule } from '../user/user.module';
import { TweetController } from './tweet.controller/tweet';
import { CommentController } from './tweet.controller/comment';
import { TweetService } from './tweet.service/tweet';
import { TweetCommentService } from './tweet.service/comment';
import { ProjectModule } from '../project/project.module';
import { EventModule } from '../event/event.module';
import { NotificationModule } from '../notification/notification.module';
import { TweetMetaService } from './tweet.service/tweet-meta';
import { DatabaseModule } from '@core/database/database.module';
import { TweetHashtagService } from './tweet.service/hashtag';
import { HashtagsController } from './tweet.controller/hashtags';
import { TweetAttachService } from './tweet.service/attach';
import { TweetActionService } from './tweet.service/action';
import { SocialModule } from '../social/social.module';
import { ProductModule } from '../product/product.module';
import { DaoModule } from '../dao/dao.module';
import { GeneralModule } from '../general/general.module';
import { WebScanModule } from '@modules/web_scan/web_scan.module';
import { PromotionModule } from '@modules/promotion/promotion.module';
import { S3Module } from '@core/utils/aws-s3/s3.module';
import { TweetCacheService } from './tweet.service/cache.service';

@Module({
  imports: [
    DatabaseModule,
    CloudinaryModule,
    WebScanModule,
    S3Module,
    forwardRef(() => DaoModule),
    forwardRef(() => UserModule),
    forwardRef(() => EventModule),
    forwardRef(() => SocialModule),
    forwardRef(() => ProjectModule),
    forwardRef(() => ProductModule),
    forwardRef(() => GeneralModule),
    forwardRef(() => NotificationModule),
    forwardRef(() => PromotionModule),
  ],
  controllers: [CommentController, TweetController, HashtagsController],
  providers: [
    TweetService,
    TweetAttachService,
    TweetActionService,
    TweetCommentService,
    TweetMetaService,
    TweetHashtagService,
    TweetCacheService,
    ...tweetProviders,
  ],
  exports: [
    TweetAttachService,
    TweetActionService,
    TweetService,
    ...tweetProviders,
  ],
})
export class TweetModule {}
