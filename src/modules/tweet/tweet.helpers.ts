import { ATTACH } from '@core/constants/attach';
import { DATABASE_CONSTANTS } from '@core/constants/database';
import { DATABASE_ENUM_TYPE } from '@core/constants/enum.types';
import { includeStandardUser } from 'src/modules/user/user.helpers';
import sequelize, { FindOptions } from 'sequelize';
import { ProjectionAlias } from 'sequelize/types';
import { firstUpperCase } from '../general/general.helpers';
import { includeLikeExist, includeLikesCount } from '../social/social.helpers';
import { Tweet } from './tweet.model';
import { PROJECT_URLS } from '@core/helpers/urls';
import sanitizeHtml from 'sanitize-html';
import { isEthereumAddress, isUUID } from 'class-validator';
import { User } from '@modules/user/user.model';
import Autolinker from 'autolinker';

export const transformTweetMessage = (
  string: string,
  userDict: { [key: string]: string }
) => {
  const linkedString: any = Autolinker.link(string, {
    truncate: 140,
    newWindow: true,
  });

  const updatedString = linkedString.replaceAll(
    /@\[[^\]]*]\([^)]*\)/g,
    (val: string) => {
      const address = val?.match(/[^(]+(?=\))/g)[0];
      const display = val?.split(']')[0].slice(2);
      const userLink = PROJECT_URLS.PROFILE_USER(address);
      const nickname = userDict[address];

      if (!isEthereumAddress(address)) {
        return `<a href="#${display}">${display}</a>`;
      }

      const htmlLink = !nickname
        ? `<a style="text-decoration:line-through;" href="${userLink}">${display}</a>`
        : `<a href="${userLink}">${'@' + nickname}</a>`;

      return htmlLink;
    }
  );

  return sanitizeHtml(updatedString, {
    allowedTags: ['a'],
    allowedAttributes: {
      a: ['href', 'target'],
    },
  });
};

export const getTweetMeta = (string) => {
  const mentions = string
    ?.match(/@\[[^\]]*]\([^)]*\)/g)
    ?.map((val: string) => val.split(']')[1].slice(1, -1))
    ?.filter((val: string) => isEthereumAddress(val));

  const hashtags = string
    ?.match(/@\[[^\]]*]\([^)]*\)/g)
    ?.filter((val: string) => isUUID(val?.split(']')[1]?.slice(1, -1)))
    ?.map((val: string) => val?.match(/\[[^\]]*]/g)[0]?.slice(2, -1));

  return { mentions, hashtags };
};

export const attachToArray = () => {
  const attaches = [];
  for (const index in ATTACH) attaches.push(ATTACH[index]);
  return attaches;
};

export const includeTweet = (userId: string = null) => {
  const args: ProjectionAlias[] = [
    includeLikesCount(DATABASE_CONSTANTS.TWEET),
    [
      sequelize.literal(`(
          SELECT 
            COUNT(*) 
          FROM 
            tweets_view 
          WHERE 
            tweets_view."tweetId" = "Tweet".id
        )`),
      'viewedCount',
    ],
    [
      sequelize.literal(`(
        SELECT 
          COUNT(*) 
        FROM 
          tweets_comments 
        WHERE 
          tweets_comments."tweetId" = "Tweet".id
      )`),
      'commentsCount',
    ],
    [
      sequelize.literal(`(
      SELECT 
        COUNT(*) 
      FROM 
        tweets 
      WHERE 
        tweets."retweetId" = "Tweet"."id"
    )`),
      'retweetsCount',
    ],
  ];
  if (userId) {
    args.push(includeLikeExist(DATABASE_CONSTANTS.TWEET, userId));
  }

  return args;
};

export const includeTweetsCount = (
  entityType: DATABASE_ENUM_TYPE
): ProjectionAlias => [
  sequelize.literal(
    `(
        SELECT 
          COUNT(*) 
        FROM 
          tweets 
        WHERE 
          tweets."entityId" = "${firstUpperCase(entityType)}".id 
          AND tweets."entityType" = '${entityType}'
      )
      `
  ),
  'tweetsCount',
];

export const defaultOptions = (userId: string) =>
  ({
    attributes: {
      exclude: ['userId'],
      include: includeTweet(userId),
    },
    order: [
      ['isPinned', 'DESC'],
      ['createdAt', 'DESC'],
    ],
    include: [
      includeStandardUser({ required: true, foreignKey: 'userId' }),
      {
        model: User,
        as: 'mentions',
        attributes: ['id', 'nickname', 'publicAddress'],
      },
    ],
  } as FindOptions<Tweet>);

export const TweetDTO = (
  tweet: Tweet,
  attach: object,
  attachObject: object
) => ({
  attachObject,
  ...tweet.get(),
  attach,
  likeExist: tweet.getDataValue('likeExist'),
});

export const includeHashtagsMentionsCount = (): ProjectionAlias => {
  const args: ProjectionAlias = [
    sequelize.literal(
      `(
        SELECT 
          count(*)
        FROM 
          tweets_hashtags 
        WHERE 
          tweets_hashtags."hashtagId" = "TweetHashtag".id
      )`
    ),
    'mentionsCount',
  ];

  return args;
};
