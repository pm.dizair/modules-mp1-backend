import {
  BelongsTo,
  BelongsToMany,
  Column,
  DataType,
  ForeignKey,
  HasMany,
  Model,
  Table,
} from 'sequelize-typescript';
import { Project } from '../project/models/project.model';
import { AttachSchemaType } from './dto/attach.schema';
import { User } from '../user/user.model';
import { Like } from '../social/social.model';
import { DATABASE_CONSTANTS } from '@core/constants/database';
import { Dao } from '../dao/dao.model';
import { ADVANCED_ENTITIES_ENUM_TYPE } from '@core/constants/enum.types';

@Table({ tableName: 'tweets_mentions', updatedAt: false, createdAt: false })
export class TweetMentions extends Model {
  @ForeignKey(() => Tweet)
  @Column({ type: DataType.UUID })
  tweetId: string;

  @ForeignKey(() => User)
  @Column({ type: DataType.UUID })
  userId: string;
}

@Table({ tableName: 'tweets_hashtags', updatedAt: false, createdAt: false })
export class TweetHashtags extends Model {
  @ForeignKey(() => Tweet)
  @Column({ type: DataType.UUID })
  tweetId: string;

  @ForeignKey(() => TweetHashtag)
  @Column({ type: DataType.UUID })
  hashtagId: string;
}

@Table({ tableName: 'tweet_hashtags', updatedAt: false, createdAt: false })
export class TweetHashtag extends Model {
  @Column({
    defaultValue: DataType.UUIDV4,
    primaryKey: true,
    type: DataType.UUID,
  })
  id: string;

  @Column({ type: DataType.STRING })
  name: string;

  @BelongsToMany(() => Tweet, () => TweetHashtags)
  tweets: Tweet[];
}

@Table({ tableName: 'tweets', deletedAt: true })
export class Tweet extends Model {
  @Column({
    defaultValue: DataType.UUIDV4,
    primaryKey: true,
    type: DataType.UUID,
  })
  id: string;

  @Column({ type: DataType.UUID })
  entityId: string;

  @Column({ type: DataType.STRING })
  entityType: ADVANCED_ENTITIES_ENUM_TYPE;

  @Column({ type: DataType.TEXT })
  text: string;

  @Column({ type: DataType.STRING })
  media: string;

  @Column({ type: DataType.STRING, allowNull: true })
  attachName: string;

  @ForeignKey(() => Tweet)
  @Column({ type: DataType.UUID })
  retweetId: string;

  @BelongsTo(() => Tweet)
  retweet: Tweet;

  @ForeignKey(() => Project)
  @Column({ type: DataType.UUID, allowNull: true })
  shareProjectId: string;

  @Column({ type: DataType.JSONB, allowNull: true })
  attach: AttachSchemaType;

  @Column({ type: DataType.BOOLEAN, defaultValue: false })
  isPinned: boolean;

  @Column({ type: DataType.BOOLEAN, defaultValue: false })
  isEdited: boolean;

  @ForeignKey(() => User)
  @Column({ type: DataType.UUID })
  userId: string;

  @BelongsTo(() => User)
  user: User;

  @HasMany(() => TweetComment)
  comments: TweetComment[];

  @HasMany(() => TweetView)
  views: TweetComment[];

  @HasMany(() => Like, {
    constraints: true,
    foreignKey: 'entityId',
    scope: {
      entityType: DATABASE_CONSTANTS.TWEET,
    },
  })
  likes: Like[];

  @BelongsToMany(() => User, () => TweetMentions)
  mentions: User[];

  @BelongsTo(() => Project, {
    constraints: true,
    foreignKey: 'entityId',
    scope: {
      entityType: DATABASE_CONSTANTS.PROJECT,
    },
  })
  project: Project;

  @BelongsTo(() => Dao, {
    constraints: true,
    foreignKey: 'entityId',
    scope: {
      entityType: DATABASE_CONSTANTS.DAO,
    },
  })
  dao: Dao;
}

@Table({ tableName: 'tweets_comments' })
export class TweetComment extends Model {
  @Column({
    defaultValue: DataType.UUIDV4,
    primaryKey: true,
    type: DataType.UUID,
  })
  id: string;
  @ForeignKey(() => User)
  @Column({ type: DataType.UUID })
  userId: string;

  @ForeignKey(() => Tweet)
  @Column({ type: DataType.UUID })
  tweetId: string;

  @Column({ type: DataType.TEXT })
  text: string;

  @Column
  media: string;

  @BelongsTo(() => User)
  user: User;
}

@Table({ tableName: 'tweets_retweets', updatedAt: false })
export class Retweet extends Model {
  @Column({
    defaultValue: DataType.UUIDV4,
    primaryKey: true,
    type: DataType.UUID,
  })
  id: string;
  @ForeignKey(() => User)
  @Column({ type: DataType.UUID })
  userId: string;

  @ForeignKey(() => Tweet)
  @Column({ type: DataType.UUID })
  tweetId: string;
}

@Table({
  tableName: 'tweets_view',
  createdAt: true,
  updatedAt: false,
})
export class TweetView extends Model {
  @Column({
    defaultValue: DataType.UUIDV4,
    primaryKey: true,
    type: DataType.UUID,
  })
  id: number;

  @ForeignKey(() => Tweet)
  @Column({
    type: DataType.UUID,
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
    allowNull: false,
  })
  tweetId: string;

  @ForeignKey(() => User)
  @Column({
    type: DataType.UUID,
    allowNull: false,
  })
  userId: string;
}
