import {
  IsIn,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
  ValidateNested,
} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { AttachSchema, AttachSchemaType } from './attach.schema';
import { DATABASE_CONSTANTS } from '@core/constants/database';
import { DATABASE_ENUM_TYPE } from '@core/constants/enum.types';
import { Tweet } from '../tweet.model';
export class CreateTweetSchema {
  @ApiProperty()
  @IsOptional()
  text?: string;

  @ApiProperty()
  @IsOptional()
  attach?: string;
}

export class TweetSchema {
  @ApiProperty()
  @IsNotEmpty()
  userId: string;

  @ApiProperty()
  @IsNotEmpty()
  entityId: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsIn(Object.values(DATABASE_CONSTANTS))
  entityType: DATABASE_ENUM_TYPE;
}

export class TweetShareSchema extends TweetSchema {
  @ApiProperty()
  @ValidateNested()
  @Type(() => AttachSchema)
  attach: AttachSchemaType;
  @IsOptional()
  @ApiProperty()
  text: string;
}

export class TweetRetweetSchema extends TweetSchema {
  @ApiProperty()
  @IsNotEmpty()
  retweetId: string;

  @ApiProperty()
  @IsOptional()
  text?: string;

  @ApiProperty()
  @IsNotEmpty()
  attach: AttachSchemaType;
}

export class TweetPromoteSchema extends TweetRetweetSchema {
  @ApiProperty()
  @IsNotEmpty()
  promotionId: string;
}

export class TweetAttachSchema extends Tweet {
  display_text?: string;
  isPromoted?: boolean;
  attachObject?: TweetAttachSchema;
}

export class TweetSearchSchema {
  @ApiProperty()
  @IsString()
  search: string;

  @ApiProperty()
  @IsNumber()
  limit: number;

  @ApiProperty()
  @IsNumber()
  offset: number;
}

export class RefreshTweetSchema {
  @ApiProperty()
  @IsNotEmpty()
  cache: string;

  @ApiProperty()
  @IsOptional()
  is_error: string;
}
