import { NFT_REPOSITORY } from '@core/constants/repository';
import { Nft } from './nft.model';

export const nftProviders = [
  {
    provide: NFT_REPOSITORY,
    useValue: Nft,
  },
];
