import { DATABASE_ENUM_TYPE } from '@core/constants/enum.types';
import { CACHE_MANAGER, Inject } from '@nestjs/common';
import { Cache } from 'cache-manager';
import { nanoid } from 'nanoid';

interface TweetCacheDTO {
  entityId: string;
  entityType: DATABASE_ENUM_TYPE;
  userId: string;
  text: string | null;

  //for testing
  startTime: number;
  size: number | null;
}

export class TweetCacheService {
  private expared = 1800;
  constructor(
    @Inject(CACHE_MANAGER)
    private readonly cacheManager: Cache
  ) {}

  async cacheTweet(
    entityId: string,
    entityType: DATABASE_ENUM_TYPE,
    userId: string,
    text: string,
    size: number = null
  ) {
    const cache_key = nanoid(12);
    await this.cacheManager.set<string>(
      cache_key,
      JSON.stringify({
        entityId,
        entityType,
        userId,
        text,
        size,
        startTime: Date.now(),
      }),
      {
        ttl: this.expared,
      }
    );

    return cache_key;
  }

  async getCachedTweet(cache_key: string) {
    const cachedData = await this.cacheManager.get<string>(cache_key);
    if (cachedData) {
      const cachedTweet: TweetCacheDTO = JSON.parse(cachedData);
      return cachedTweet;
    }
    return null;
  }

  async removeCached(cache_key: string) {
    const cachedData = await this.cacheManager.get<string>(cache_key);
    if (cachedData) {
      this.cacheManager.del(cache_key);
      return JSON.parse(cachedData);
    }
    return null;
  }
}
