import { forwardRef, Module } from '@nestjs/common';
import { NftService } from './services/nft.service';
import { NftController } from './nft.controller';
import { nftProviders } from './nft.providers';
import { AuthModule } from '@core/auth/auth.module';
import { DatabaseModule } from '@core/database/database.module';
import { NotificationModule } from '../notification/notification.module';
import { UserModule } from '../user/user.module';
import { NftMoralisService } from './services/nft.moralis';
import { SocialModule } from '../social/social.module';
import { GeneralModule } from '../general/general.module';
@Module({
  imports: [
    AuthModule,
    DatabaseModule,
    forwardRef(() => UserModule),
    forwardRef(() => SocialModule),
    forwardRef(() => GeneralModule),
    forwardRef(() => NotificationModule),
  ],
  controllers: [NftController],
  providers: [NftService, NftMoralisService, ...nftProviders],
  exports: [NftService, NftMoralisService],
})
export class NftModule {}
