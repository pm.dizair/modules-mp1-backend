import ERC721 from './ERC721';
import ERC20 from './ERC20';
import Promotion from './Promotion';

export const AbiProjects = {
  CRYSTALL_OF_POWER: ERC721,
  SHARD: ERC721,

  PROJECT: ERC721,
  PRODUCT: ERC20,
  DAO: ERC20,
  Promotion,
};
