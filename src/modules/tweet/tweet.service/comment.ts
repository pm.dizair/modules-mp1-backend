import { Inject, Injectable } from '@nestjs/common';
import { Tweet, TweetComment } from '../tweet.model';
import { CommentSchema } from '../dto/comment.schema';
import {
  SEQUELIZE_REPOSITORY,
  TWEET_COMMENT_REPOSITORY,
} from '@core/constants/repository';
import { Attributes, FindOptions } from 'sequelize';
import { includeMinUser } from 'src/modules/user/user.helpers';
import { Sequelize } from 'sequelize';

@Injectable()
export class TweetCommentService {
  constructor(
    @Inject(SEQUELIZE_REPOSITORY)
    private readonly sequelize: Sequelize,
    @Inject(TWEET_COMMENT_REPOSITORY)
    private readonly commentModel: typeof TweetComment
  ) {}

  async getCommentsCount(id: string) {
    const { count } = (
      await this.sequelize.query(`(
      SELECT 
        COUNT(*) 
      FROM 
        tweets_comments 
      WHERE 
        "tweetId" = '${id}'
    )`)
    )[0][0] as { count: number };

    return Number(count);
  }

  async getByIdAndUser(id: string, userId: string) {
    return this.commentModel.findOne({ where: { id, userId } });
  }

  async findAndCountAll(tweetId: string, page: number, limit: number) {
    const query: FindOptions<TweetComment> = {
      offset: (page - 1) * limit,
      limit,
      where: { tweetId },
      order: [['createdAt', 'DESC']],
      include: includeMinUser(),
    };
    return this.commentModel.findAndCountAll(query);
  }

  async findOne(id: string) {
    const query: FindOptions<TweetComment> = {
      where: { id },
      order: [['createdAt', 'DESC']],
      include: includeMinUser(),
    };
    return this.commentModel.findOne(query);
  }

  async create(
    tweetId: string,
    body: CommentSchema,
    userId: string,
    media: string = null
  ) {
    const comment = await this.commentModel.create<TweetComment>({
      ...body,
      tweetId,
      userId,
      media,
    });
    return await this.findOne(comment.id);
  }

  async delete(id: string) {
    return this.commentModel.destroy({ where: { id } });
  }

  async update(id: string, data: Attributes<Tweet>, userId: string) {
    const [numberOfAffectedRows, [updatedRow]] = await this.commentModel.update(
      { ...data },
      { where: { id, userId }, returning: true }
    );
    return { numberOfAffectedRows, updatedRow };
  }
}
