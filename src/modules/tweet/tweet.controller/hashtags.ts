import { ApiTags } from '@nestjs/swagger';
import { Controller, UseGuards, Get, Query } from '@nestjs/common';
import { JwtAuthGuard } from '@auth/jwt.guard';
import { TweetHashtagService } from '../tweet.service/hashtag';
import { GetAllHashtagsSchema } from '../dto/hashtag.schema';

@ApiTags('tweet-hashtags')
@Controller('hashtags')
export class HashtagsController {
  constructor(private readonly tweetHashtagService: TweetHashtagService) {}

  @UseGuards(JwtAuthGuard)
  @Get()
  async getAllHashtags(@Query() dto: GetAllHashtagsSchema) {
    return this.tweetHashtagService.getAllHashtags(dto);
  }
}
