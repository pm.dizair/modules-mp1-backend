import { ATTACH } from '@core/constants/attach';
import {
  NFT_REPOSITORY,
  SEQUELIZE_REPOSITORY,
} from '@core/constants/repository';
import { BadRequestException, Inject, Injectable } from '@nestjs/common';
import { Sequelize } from 'sequelize-typescript';
import { ShareNFTSchema } from 'src/modules/social/dto/share.dto';
import { ShareService } from 'src/modules/social/services/social.share';
import { NFTSchema } from '../dto/nft.dto';
import { includeLikeExistNFT } from '../nft.helpers';
import { Nft } from '../nft.model';

@Injectable()
export class NftService {
  constructor(
    @Inject(NFT_REPOSITORY)
    private readonly nftModel: typeof Nft,
    @Inject(SEQUELIZE_REPOSITORY)
    private readonly sequelize: Sequelize,
    private readonly shareService: ShareService
  ) {}

  async getSimpleById(id: string) {
    return await this.nftModel.findOne({ where: { id } });
  }

  async findNfts(nfts: NFTSchema[], userId: string = null) {
    const nfts_list = [];
    for (const { tokenAddress, tokenId } of nfts) {
      const nftEqual = await this.nftModel.findOne({
        where: { tokenAddress, tokenId },
        attributes: {
          include: [...includeLikeExistNFT({ tokenAddress, tokenId }, userId)],
        },
      });
      if (nftEqual) nfts_list.push(nftEqual);
    }
    return nfts_list;
  }

  async findOne({ tokenAddress, tokenId }: NFTSchema) {
    return this.nftModel.findOne({ where: { tokenAddress, tokenId } });
  }

  async findOrCreate({ tokenAddress, tokenId }: NFTSchema) {
    return this.nftModel.findOrCreate({
      where: { tokenAddress, tokenId: tokenId?.toString() },
    });
  }

  async shareNFT(body: ShareNFTSchema, userId: string) {
    const { nft } = body;

    const shareNFT = await this.findOrCreate(nft);
    if (!shareNFT) throw new BadRequestException('NFT not found');

    return await this.shareService.share(
      body,
      {
        ...nft,
        objectType: ATTACH.NFT,
      },
      userId
    );
  }

  async getMostLiked() {
    const rows = (
      await this.sequelize.query(`(
        SELECT 
          "tokenAddress" AS token_address, 
          "tokenId" AS token_id 
        FROM 
          nfts 
        ORDER BY 
          (
            SELECT 
              COUNT(*) 
            FROM 
              likes 
            WHERE 
              likes."entityId" = nfts."id"
          ) DESC 
        LIMIT 
          100
      )`)
    )[0];
    return rows;
  }
}
