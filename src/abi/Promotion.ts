import { AbiItem } from 'web3-utils';

export default [
  {
    inputs: [
      {
        internalType: 'address',
        name: 'crystal_address_',
        type: 'address',
      },
      {
        internalType: 'address',
        name: 'lexor_address_',
        type: 'address',
      },
      {
        internalType: 'address',
        name: 'owner_of_',
        type: 'address',
      },
      {
        internalType: 'address',
        name: 'dao_factory_address_',
        type: 'address',
      },
    ],
    stateMutability: 'nonpayable',
    type: 'constructor',
  },
  {
    anonymous: false,
    inputs: [
      {
        indexed: false,
        internalType: 'uint256',
        name: 'uid',
        type: 'uint256',
      },
      {
        indexed: false,
        internalType: 'string',
        name: 'object_id',
        type: 'string',
      },
      {
        indexed: false,
        internalType: 'uint256',
        name: 'budget',
        type: 'uint256',
      },
      {
        indexed: false,
        internalType: 'uint256',
        name: 'post_price',
        type: 'uint256',
      },
      {
        indexed: false,
        internalType: 'uint256',
        name: 'start_time',
        type: 'uint256',
      },
      {
        indexed: false,
        internalType: 'uint256',
        name: 'end_time',
        type: 'uint256',
      },
      {
        indexed: false,
        internalType: 'bool',
        name: 'resolved',
        type: 'bool',
      },
    ],
    name: 'created',
    type: 'event',
  },
  {
    anonymous: false,
    inputs: [
      {
        indexed: false,
        internalType: 'uint256',
        name: 'uid',
        type: 'uint256',
      },
      {
        indexed: false,
        internalType: 'uint256',
        name: 'price',
        type: 'uint256',
      },
      {
        indexed: false,
        internalType: 'uint256',
        name: 'start_time',
        type: 'uint256',
      },
      {
        indexed: false,
        internalType: 'uint256',
        name: 'end_time',
        type: 'uint256',
      },
      {
        indexed: false,
        internalType: 'enum Promotion.ProposalType',
        name: 'proposal_type',
        type: 'uint8',
      },
    ],
    name: 'proposalCreated',
    type: 'event',
  },

  {
    anonymous: false,
    inputs: [
      {
        indexed: false,
        internalType: 'uint256',
        name: 'uid',
        type: 'uint256',
      },
      {
        indexed: false,
        internalType: 'bool',
        name: 'submited',
        type: 'bool',
      },
    ],
    name: 'proposalResolved',
    type: 'event',
  },

  {
    anonymous: false,
    inputs: [
      {
        indexed: false,
        internalType: 'uint256',
        name: 'uid',
        type: 'uint256',
      },
      {
        indexed: false,
        internalType: 'address[]',
        name: 'dao_addresses',
        type: 'address[]',
      },
      {
        indexed: false,
        internalType: 'uint256[]',
        name: 'percentages',
        type: 'uint256[]',
      },
      {
        indexed: false,
        internalType: 'string',
        name: 'object_id',
        type: 'string',
      },
      {
        indexed: false,
        internalType: 'uint256',
        name: 'dao_value',
        type: 'uint256',
      },
      {
        indexed: false,
        internalType: 'address[]',
        name: 'receivers',
        type: 'address[]',
      },
      {
        indexed: false,
        internalType: 'uint256',
        name: 'watcher_value',
        type: 'uint256',
      },

      {
        indexed: false,
        internalType: 'uint256',
        name: 'return_value',
        type: 'uint256',
      },
    ],
    name: 'resolved',
    type: 'event',
  },
  {
    anonymous: false,
    inputs: [
      {
        indexed: false,
        internalType: 'address',
        name: 'eth_address',
        type: 'address',
      },
      {
        indexed: false,
        internalType: 'bool',
        name: 'voice',
        type: 'bool',
      },
    ],
    name: 'voiceSubmited',
    type: 'event',
  },
  {
    inputs: [
      {
        internalType: 'address',
        name: 'owner_of_',
        type: 'address',
      },
    ],
    name: 'setOwner',
    outputs: [],
    stateMutability: 'nonpayable',
    type: 'function',
  },
  {
    inputs: [
      {
        internalType: 'bool',
        name: 'paused_',
        type: 'bool',
      },
    ],
    name: 'setPaused',
    outputs: [],
    stateMutability: 'nonpayable',
    type: 'function',
  },
  {
    inputs: [
      {
        internalType: 'uint256',
        name: 'price_',
        type: 'uint256',
      },
      {
        internalType: 'enum Promotion.ProposalType',
        name: 'proposal_type_',
        type: 'uint8',
      },
    ],
    name: 'createProposal',
    outputs: [],
    stateMutability: 'nonpayable',
    type: 'function',
  },
  {
    inputs: [
      {
        internalType: 'uint256',
        name: 'uid_',
        type: 'uint256',
      },
      {
        internalType: 'bool',
        name: 'voice_',
        type: 'bool',
      },
    ],
    name: 'vote',
    outputs: [],
    stateMutability: 'nonpayable',
    type: 'function',
  },
  {
    inputs: [
      {
        internalType: 'uint256',
        name: 'uid_',
        type: 'uint256',
      },
    ],
    name: 'resolveProposal',
    outputs: [],
    stateMutability: 'nonpayable',
    type: 'function',
  },
  {
    inputs: [],
    name: 'getPrice',
    outputs: [
      {
        internalType: 'uint256',
        name: '',
        type: 'uint256',
      },
    ],
    stateMutability: 'view',
    type: 'function',
    constant: true,
  },
  {
    inputs: [
      {
        internalType: 'string',
        name: 'object_id_',
        type: 'string',
      },
      {
        internalType: 'uint256',
        name: 'amount_',
        type: 'uint256',
      },
      {
        internalType: 'uint256',
        name: 'start_time_',
        type: 'uint256',
      },
      {
        internalType: 'uint256',
        name: 'period_',
        type: 'uint256',
      },
    ],
    name: 'create',
    outputs: [],
    stateMutability: 'nonpayable',
    type: 'function',
  },

  {
    inputs: [
      {
        internalType: 'uint256',
        name: 'uid',
        type: 'uint256',
      },
      {
        internalType: 'address[]',
        name: 'dao_addresses_',
        type: 'address[]',
      },
      {
        internalType: 'uint256[]',
        name: 'percentages_',
        type: 'uint256[]',
      },
      {
        internalType: 'address[]',
        name: 'receivers_',
        type: 'address[]',
      },
    ],
    name: 'resolve',
    outputs: [],
    stateMutability: 'nonpayable',
    type: 'function',
  },
] as AbiItem[];
