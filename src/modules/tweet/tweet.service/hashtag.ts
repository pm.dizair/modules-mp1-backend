import { Inject, Injectable } from '@nestjs/common';
import { Tweet, TweetHashtag } from '../tweet.model';
import { TWEET_HASHTAGS_REPOSITORY } from '@core/constants/repository';
import { GetAllHashtagsSchema } from '../dto/hashtag.schema';
import sequelize from 'sequelize';

import { includeHashtagsMentionsCount } from '../tweet.helpers';

@Injectable()
export class TweetHashtagService {
  constructor(
    @Inject(TWEET_HASHTAGS_REPOSITORY)
    private readonly tweetHashtagModel: typeof TweetHashtag
  ) {}

  async getAllHashtags(dto: GetAllHashtagsSchema) {
    const { rows, count } = await this.tweetHashtagModel.findAndCountAll({
      where: {
        name: {
          [sequelize.Op.like]: `%${dto.query}%`,
        },
      },
      limit: dto.limit,
      include: [
        {
          model: Tweet,
        },
      ],
      attributes: ['id', 'name', includeHashtagsMentionsCount()],
    });

    return {
      count,
      rows: rows.map((item) => ({
        ...item.get(),
        display: '#' + item.get().name,
      })),
    };
  }
}
