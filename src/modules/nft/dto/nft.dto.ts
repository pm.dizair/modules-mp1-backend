import { IsEthAddress } from '@core/validators/is-eth-contract';
import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import {
  ArrayMinSize,
  IsNotEmpty,
  MaxLength,
  MinLength,
  ValidateNested,
} from 'class-validator';
export class NFTSchema {
  @ApiProperty()
  @IsNotEmpty()
  @MinLength(42)
  @MaxLength(42)
  @IsEthAddress()
  tokenAddress: string;

  @ApiProperty()
  @IsNotEmpty()
  tokenId: string;
}

export class InfoSchema {
  @ApiProperty()
  @ArrayMinSize(1)
  @ValidateNested()
  @Type(() => NFTSchema)
  ids: NFTSchema[];
}
