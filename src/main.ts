import { NestFactory } from '@nestjs/core';
import * as Sentry from '@sentry/node';
import { ConfigService } from '@nestjs/config';
import { ValidationPipe } from '@nestjs/common';
import { initSwagger } from '@core/config/swagger';
import { AppModule } from './app.module';
import { RedisIoAdapter } from '@core/adapter/redis.io.adapter';
import { config } from 'aws-sdk';

loadEnv();
printEnv();

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const configService = app.get(ConfigService);
  const redisIoAdapter = new RedisIoAdapter(app);

  await redisIoAdapter.connectToRedis(configService);
  app.useWebSocketAdapter(redisIoAdapter);

  config.update({
    region: configService.get('AWS_REGION'),
    accessKeyId: configService.get('AWS_ACCESS_KEY_ID'),
    secretAccessKey: configService.get('AWS_SECRET_ACCESS_KEY'),
  });

  Sentry.init({
    dsn: configService.get<string>('SENTRY_DNS'),
    tracesSampleRate: 1.0,
    enabled: true,
    // enabled: configService.get('NODE_ENV') !== 'dev',
  });

  app.enableCors({
    origin: String(process.env.CORS_ORIGIN).split(','),
    credentials: true,
  });

  app.setGlobalPrefix('api/v2');
  app.useGlobalPipes(new ValidationPipe({ transform: true, whitelist: true }));
  app.getHttpAdapter().getInstance().disable('x-powered-by');
  initSwagger(app);

  await app.listen(configService.get('APP_PORT'));
}

bootstrap();

function loadEnv() {
  require('dotenv').config({ path: `./.env` });
}

function printEnv() {
  console.log('Some major env variable:');
  console.log('   APP_PORT: ' + process.env.APP_PORT);
  console.log('   DB_HOST: ' + process.env.DB_HOST);
  console.log('   DB_NAME: ' + process.env.DB_NAME);
  console.log('   DB_USERNAME: ' + process.env.DB_USERNAME);
  console.log('   REDIS_HOST: ' + process.env.REDIS_HOST);
  console.log('   MORALIS_SERVER_URL: ' + process.env.MORALIS_SERVER_URL);
  console.log('   CLOUDINARY_CLOUD_NAME: ' + process.env.CLOUDINARY_CLOUD_NAME);

  const fs = require('fs');
  if (fs.existsSync('/app/version.json')) {
    const version = fs.readFileSync('/app/version.json');
    console.log('Version: ' + version);
  } else {
    console.log('Version is not defined');
  }
}
