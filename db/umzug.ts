import {join} from 'path';
import { Sequelize } from 'sequelize-typescript';
// @ts-ignore
import { SequelizeStorage, Umzug } from 'umzug';
import * as dotenv from 'dotenv';

dotenv.config();

const sequelize = new Sequelize({
    dialect: 'postgres',
    host: process.env.DB_HOST || 'localhost',
    port: +process.env.DB_PORT || 5432,
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
});

export const migrator = new Umzug({
    storage: new SequelizeStorage({ sequelize }),
    context: sequelize.getQueryInterface(),
    logger: console,
    migrations: {
        glob: ['migrations/*.ts', { cwd: join(__dirname, '../../') }],
    },
});

export type Migration = typeof migrator._types.migration;
