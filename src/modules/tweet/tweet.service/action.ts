import { forwardRef, Inject, Injectable } from '@nestjs/common';
import { Tweet, TweetView } from '../tweet.model';
import { defaultOptions, TweetDTO } from '../tweet.helpers';
import { CreationAttributes } from 'sequelize/types';
import { Model } from 'sequelize-typescript';
import {
  SEQUELIZE_REPOSITORY,
  TWEET_REPOSITORY,
  TWEET_VIEW_REPOSITORY,
} from '@core/constants/repository';
import { TweetAttachService } from './attach';
import { ShareService } from 'src/modules/social/services/social.share';
import { ShareTweetBodySchema } from 'src/modules/social/dto/share.dto';
import { TweetShareSchema } from '../dto/tweet.schema';
import { Sequelize } from 'sequelize';

@Injectable()
export class TweetActionService {
  constructor(
    @Inject(SEQUELIZE_REPOSITORY)
    private readonly sequelize: Sequelize,
    @Inject(TWEET_REPOSITORY)
    private readonly tweetModel: typeof Tweet,
    @Inject(TWEET_VIEW_REPOSITORY)
    private readonly tweetViewModel: typeof TweetView,
    @Inject(forwardRef(() => TweetAttachService))
    private readonly tweetAttachService: TweetAttachService,
    @Inject(forwardRef(() => ShareService))
    private readonly shareService: ShareService
  ) {}

  async retweetOrQuote(
    tweetId: string,
    body: ShareTweetBodySchema,
    userId: string
  ): Promise<Tweet[]> {
    const tweet = await this.tweetModel.findOne({
      where: { id: tweetId },
    });
    return await this.shareService.retweet(body, tweet.id, userId);
  }

  async bulkCreateAndSend(
    data: CreationAttributes<Model<Tweet, TweetShareSchema>>[],
    userId: string
  ): Promise<Tweet[]> {
    const tweets = [];
    const createdTweets = await this.tweetModel.bulkCreate(data);
    const tweetsDB = await this.tweetModel.findAll({
      where: { id: createdTweets.map(({ id }) => id) },
      ...defaultOptions(userId),
    });

    for (const tweet of tweetsDB) {
      const { attach, attachObject } = await this.tweetAttachService.getAttach(
        tweet,
        userId
      );
      tweets.push(TweetDTO(tweet, attach, attachObject));
    }

    return tweets;
  }

  async pin(id: string): Promise<number> {
    const tweet = await this.tweetModel.findOne({
      where: { id },
    });
    if (tweet.isPinned) {
      await tweet.update({ isPinned: false });
      return -1;
    } else {
      await tweet.update({ isPinned: true });
      return 1;
    }
  }

  async updateIsView(ids: string[], userId: string): Promise<string[]> {
    const created = [];

    const viewTweet = async (tweetId: string) => {
      const [, isCreated] = await this.tweetViewModel.findOrCreate({
        where: { tweetId, userId },
      });
      if (isCreated) created.push(tweetId);
    };

    for (const tweetId of ids) {
      const tweet = await this.tweetModel.findOne({ where: { id: tweetId } });
      if (tweet.retweetId) viewTweet(tweet.retweetId);
      viewTweet(tweetId);
    }

    return created;
  }

  async getViewedCount(id: string) {
    const { count } = (
      await this.sequelize.query(`(
      SELECT 
        COUNT(*) 
      FROM 
        tweets_view 
      WHERE 
        "tweetId" = '${id}'
    )`)
    )[0][0] as { count: number };

    return Number(count);
  }
}
