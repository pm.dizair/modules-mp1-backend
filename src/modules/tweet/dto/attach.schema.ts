import { ATTACH } from '@core/constants/attach';
import {
  PRODUCT_LOWER_ENUM_TYPE,
  PROJECT_LOWER_ENUM_TYPE,
} from '@core/constants/enum.types';
import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional } from 'class-validator';

class TweetAttachSchema {
  @ApiProperty()
  @IsNotEmpty()
  objectType: string;
}
class TweetAttachNftSchema implements TweetAttachSchema {
  @ApiProperty()
  @IsNotEmpty()
  objectType: typeof ATTACH.NFT;

  @ApiProperty()
  @IsNotEmpty()
  tokenAddress: string;

  @ApiProperty()
  @IsNotEmpty()
  tokenId: string;
}

export class TweetAttachProjectSchema extends TweetAttachSchema {
  @ApiProperty()
  @IsNotEmpty()
  objectType: PROJECT_LOWER_ENUM_TYPE;

  @ApiProperty()
  objectId: string;
}

export class TweetAttachProductSchema extends TweetAttachSchema {
  @ApiProperty()
  @IsNotEmpty()
  objectType: PRODUCT_LOWER_ENUM_TYPE;

  @ApiProperty()
  objectId: string;
}

export class TweetAttachEventSchema extends TweetAttachSchema {
  @ApiProperty()
  @IsNotEmpty()
  objectType: typeof ATTACH.EVENT;

  @ApiProperty()
  id: string;
}

export class TweetAttachMediaSchema extends TweetAttachSchema {
  @ApiProperty()
  @IsNotEmpty()
  objectType: typeof ATTACH.MEDIA;

  @ApiProperty()
  urls: string[];

  @ApiProperty()
  @IsOptional()
  status?: string;
}

export class TweetAttachDAOSchema extends TweetAttachSchema {
  @ApiProperty()
  @IsNotEmpty()
  objectType: typeof ATTACH.DAO;

  @ApiProperty()
  objectId: string;
}

export class TweetAttachRepostSchema extends TweetAttachSchema {
  @ApiProperty()
  @IsNotEmpty()
  objectType: typeof ATTACH.REPOST;
}

export class TweetAttachVideoSchema extends TweetAttachSchema {
  @ApiProperty()
  @IsNotEmpty()
  objectType: typeof ATTACH.VIDEO;

  url: string;
}

export const AttachSchema =
  TweetAttachNftSchema ||
  TweetAttachProjectSchema ||
  TweetAttachEventSchema ||
  TweetAttachMediaSchema ||
  TweetAttachDAOSchema ||
  TweetAttachRepostSchema ||
  TweetAttachVideoSchema;

export type AttachSchemaType =
  | TweetAttachNftSchema
  | TweetAttachProjectSchema
  | TweetAttachProductSchema
  | TweetAttachEventSchema
  | TweetAttachMediaSchema
  | TweetAttachDAOSchema
  | TweetAttachRepostSchema
  | TweetAttachVideoSchema;
